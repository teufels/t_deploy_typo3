#!/bin/bash

##
## Variables
##
CONFIG_PATH="config/deploy"
RELEASES_PATH="releases"
RELEASES_DEVELOPMENT_PATH="${RELEASES_PATH}/development"
RELEASES_STAGING_PATH="${RELEASES_PATH}/staging"
RELEASES_PRODUCTION_PATH="${RELEASES_PATH}/production"
INSTALLER_PATH="t_installer"
TRASH="trash/"$(date +"%Y-%m-%d_%H-%M-%S")

githelper() {
    case "$1" in
        "development")
            case "$2" in
                "staging")

                logMsg "development > staging $3";

                #check current staging path
                logMsg "check current staging path";
                OLD_STAGING_PATH=$(find $RELEASES_STAGING_PATH -type d | sort | tail -n +2 | head -1 | sed 's#.*/##')

                if [ "$OLD_STAGING_PATH" = "" ];
                    then
                        exit
                    else
                        if [ "$OLD_STAGING_PATH" = "$3" ];
                            then
                                exit;
                            fi
                fi

                #rename directory
                logMsg "rename folder name from ${OLD_STAGING_PATH} to $3"
                mv -v ${RELEASES_STAGING_PATH}/${OLD_STAGING_PATH} ${RELEASES_STAGING_PATH}/$3
                sleep 1

                #checkout git from t_installer
                logMsg "checkout git from t_installer"
                git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} checkout $3
                sleep 1

                #move existing files to trash
                logMsg "move existing files to trash"
                mv -v ${RELEASES_STAGING_PATH}/$3/app/composer.phar ${TRASH}
                mv -v  ${RELEASES_STAGING_PATH}/$3/app/composer.json ${TRASH}
                mv -v ${RELEASES_STAGING_PATH}/$3/app/web/typo3conf/AdditionalConfiguration.php ${TRASH}
                sleep 1

                #add new composer.phar
                logMsg "add new composer.phar"
                curl -o ${RELEASES_STAGING_PATH}/$3/app/composer.phar https://getcomposer.org/composer.phar
                sleep 4

                #copy composer.json, install_extensions and _AdditionalConfiguration.php
                logMsg "copy files to project"
                cp -fp ${INSTALLER_PATH}/src/install_extensions.sh ${RELEASES_STAGING_PATH}/$3/app
                cp -fp ${INSTALLER_PATH}/src/composer.json ${RELEASES_STAGING_PATH}/$3/app
                cp -fp ${INSTALLER_PATH}/src/_AdditionalConfiguration.php ${RELEASES_STAGING_PATH}/$3/app/web/typo3conf
                sleep 1

                #include deploy.ini variables
                runMsg ". t_deploy/deploy.ini"
                . t_deploy/deploy.ini
                #replace php syntax in install_extensions.sh
                sed -ie "s#php web/typo3/cli_dispatch.phpsh#$t_deploy_typo3__phpbinary web/typo3/cli_dispatch.phpsh#g" ${RELEASES_STAGING_PATH}/$3/app/install_extensions.sh
                sleep 1

                #rename AdditionalConfiguration
                logMsg "rename AdditionalConfiguration"
                mv -v ${RELEASES_STAGING_PATH}/$3/app/web/typo3conf/_AdditionalConfiguration.php ${RELEASES_STAGING_PATH}/$3/app/web/typo3conf/AdditionalConfiguration.php
                sleep 1

                ;;
                *)
                errorMsg "Wrong argument [TODO]\n" \
                "'$1' => '$2' not valid\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;;
            esac
        ;;

        "staging")
            case "$2" in
                "production")
                CASE=2

                logMsg "staging > production1 ${3}";

                ##
                ## there must be exactly one staging version
                ##
                COUNT_DIRECTORIES_IN_GIVEN_PATH=$(find ${RELEASES_STAGING_PATH} -mindepth 1 -maxdepth 1 -type d | wc -l)
                if [ ${COUNT_DIRECTORIES_IN_GIVEN_PATH} = 1 ]; 
                    then
                        ##
                        ## version must be equal to ${3}
                        ##
                        FIRST_DIRECTORY_IN_GIVEN_PATH=$(find ${RELEASES_STAGING_PATH} -type d | sort | tail -n +2 | head -1 | sed 's#.*/##')
                        if [ ! "${FIRST_DIRECTORY_IN_GIVEN_PATH}" = "${3}" ]; 
                            then
                                errorMsg "First directory in '${RELEASES_STAGING_PATH}' is '${FIRST_DIRECTORY_IN_GIVEN_PATH}'.\n" \
                                "${RELEASES_STAGING_PATH}/${LAST_DIRECTORY_IN_GIVEN_PATH} != ${RELEASES_STAGING_PATH}/${3} . That's not ok."
                                exitMsg;
                                exit; 
                        fi
                    else
                        errorMsg "Count directories in '${RELEASES_STAGING_PATH}' = ${COUNT_DIRECTORIES_IN_GIVEN_PATH} != 1."
                        exitMsg;
                        exit;
                fi

                ##
                ## there can be no, one or more production versions
                ##
                COUNT_DIRECTORIES_IN_GIVEN_PATH=$(find ${RELEASES_PRODUCTION_PATH} -mindepth 1 -maxdepth 1 -type d | wc -l)
                if [ ${COUNT_DIRECTORIES_IN_GIVEN_PATH} >= 1 ]; 
                    then
                        ##
                        ## highest version must NOT be equal to ${3}
                        ##
                        LAST_DIRECTORY_IN_GIVEN_PATH=$(find ${RELEASES_PRODUCTION_PATH} -type d | sort --reverse | head -1 | sed 's#.*/##')
                        if [ "${LAST_DIRECTORY_IN_GIVEN_PATH}" = "${3}" ]; then
                            errorMsg "Last directory in '${RELEASES_PRODUCTION_PATH}' is '${LAST_DIRECTORY_IN_GIVEN_PATH}'.\n" \
                            "${RELEASES_PRODUCTION_PATH}/${LAST_DIRECTORY_IN_GIVEN_PATH} = ${RELEASES_PRODUCTION_PATH}/${3}. That's not ok."
                            exitMsg;
                            exit; 
                        fi
                    else
                        infoMsg "First deployment to production."
                fi

                ##
                ## check wether master branch exists in remote INSTALLER 
                ##
                BRANCH=$(git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH branch -a | grep "remotes/origin/${3}")
                if [ "$BRANCH" = "" ]
                    then
                        checkNotMsg "Target branch '${3}' does not exist in remote INSTALLER."
                        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n" \
                        "Please create branch '${3}' in remote INSTALLER first. \n" \
                        "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
                        exitMsg
                        exit                    
                fi

                ##
                ## fetch latest t_installer from .git
                ##
                runMsg "git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} fetch"
                #if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
                else
                    git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} fetch
                    sleep 2
                fi

                ##
                ## merge latest t_installer from .git
                ##
                runMsg "git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} merge"
                #if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
                else
                    git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} merge
                    sleep 2
                fi

                #checkout git from t_installer
                logMsg "checkout git from t_installer"
                git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} checkout ${3}
                sleep 1

                #copy directory
                logMsg "copy ${RELEASES_STAGING_PATH}/${3} to ${RELEASES_PRODUCTION_PATH}/${3}"
                cp -rp ${RELEASES_STAGING_PATH}/${3} ${RELEASES_PRODUCTION_PATH}/${3}
                # sleep 1

                #move existing files to trash
                logMsg "move existing files to trash"
                mv -v ${RELEASES_PRODUCTION_PATH}/${3}/app/composer.phar ${TRASH}
                mv -v  ${RELEASES_PRODUCTION_PATH}/${3}/app/composer.json ${TRASH}
                mv -v ${RELEASES_PRODUCTION_PATH}/${3}/app/web/typo3conf/AdditionalConfiguration.php ${TRASH}
                sleep 1

                #add new composer.phar
                logMsg "add new composer.phar"
                curl -o ${RELEASES_PRODUCTION_PATH}/${3}/app/composer.phar https://getcomposer.org/composer.phar
                sleep 4

                #copy composer.json, install_extensions and _AdditionalConfiguration.php
                logMsg "copy files to project"
                cp -fp ${INSTALLER_PATH}/src/install_extensions.sh ${RELEASES_PRODUCTION_PATH}/$3/app
                cp -fp ${INSTALLER_PATH}/src/composer.json ${RELEASES_PRODUCTION_PATH}/$3/app
                cp -fp ${INSTALLER_PATH}/src/_AdditionalConfiguration.php ${RELEASES_PRODUCTION_PATH}/$3/app/web/typo3conf
                sleep 1

                #include deploy.ini variables
                runMsg ". t_deploy/deploy.ini"
                . t_deploy/deploy.ini
                #replace php syntax in install_extensions.sh
                sed -ie "s#php web/typo3/cli_dispatch.phpsh#$t_deploy_typo3__phpbinary web/typo3/cli_dispatch.phpsh#g" ${RELEASES_PRODUCTION_PATH}/$3/app/install_extensions.sh
                sleep 1

                #rename AdditionalConfiguration
                logMsg "rename AdditionalConfiguration"
                mv -v ${RELEASES_PRODUCTION_PATH}/${3}/app/web/typo3conf/_AdditionalConfiguration.php ${RELEASES_PRODUCTION_PATH}/$3/app/web/typo3conf/AdditionalConfiguration.php
                sleep 1

                ;;
                *)
                errorMsg "Wrong argument [TODO]\n" \
                "'$1' => '$2' not valid\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;;
            esac
        ;;

        "production")
            case "$2" in
                "development")
                CASE=3

                logMsg "production > development $3";

                #
                ## t_installer/.git must be present
                ##
                if [ ! -d ${INSTALLER_PATH}/.git ]; then
                    errorMsg "Directory '${INSTALLER_PATH}/.git' does not exist\n" \
                    "Feel free to read the manual by executing 'make help'"
                    exitMsg
                    exit
                fi

                ##
                ## check wether master branch exists in local INSTALLER
                ##
                BRANCH=$(git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH branch -a | grep "remotes/origin/master")
                if [ "$BRANCH" = "" ]
                    then
                        checkNotMsg "Target branch 'master' does not exist in remote INSTALLER."
                        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n" \
                        "Very bad!. \n" \
                        "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
                        exitMsg
                        exit
                fi

                ##
                ## fetch latest t_installer from .git
                ##
                runMsg "git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} fetch"
                #if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
                else
                    git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} fetch
                    sleep 2
                fi

                ##
                ## merge latest t_installer from .git
                ##
                runMsg "git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} merge"
                #if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
                else
                    git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} merge
                    sleep 2
                fi

                ##
                ## check wether tag ${3} exists in local INSTALLER
                ##
                TAG=$(git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH tag --list "${3}")
                if [ "$TAG" = "" ]
                then
                    checkNotMsg "Tag '${3}' does not exist in 'master' in local INSTALLER."
                    errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n" \
                    "Please fetch && merge && checkout tag '${3}' in local INSTALLER first. \n" \
                    "Do _NOT_ just create tag '${3}' in local INSTALLER. \n" \
                    "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
                    exitMsg
                    exit
                fi

                #checkout git from t_installer
                logMsg "checkout git from t_installer"
                git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} checkout ${3}
                sleep 1

                #move existing files to trash
                logMsg "move existing files to trash"
                mv -v ${RELEASES_DEVELOPMENT_PATH}/development/app/composer.phar ${TRASH}
                mv -v  ${RELEASES_DEVELOPMENT_PATH}/development/app/composer.json ${TRASH}
                mv -v ${RELEASES_DEVELOPMENT_PATH}/development/app/web/typo3conf/AdditionalConfiguration.php ${TRASH}
                sleep 1

                #add new composer.phar
                logMsg "add new composer.phar"
                curl -o ${RELEASES_DEVELOPMENT_PATH}/development/app/composer.phar https://getcomposer.org/composer.phar
                sleep 4

                #copy composer.json, install_extensions and _AdditionalConfiguration.php
                logMsg "copy files to project"
                cp -fp ${INSTALLER_PATH}/src/install_extensions.sh ${RELEASES_DEVELOPMENT_PATH}/development/app
                cp -fp ${INSTALLER_PATH}/src/composer.json ${RELEASES_DEVELOPMENT_PATH}/development/app
                cp -fp ${INSTALLER_PATH}/src/_AdditionalConfiguration.php ${RELEASES_DEVELOPMENT_PATH}/development/app/web/typo3conf
                sleep 1

                #include deploy.ini variables
                runMsg ". t_deploy/deploy.ini"
                . t_deploy/deploy.ini
                #replace php syntax in install_extensions.sh
                sed -ie "s#php web/typo3/cli_dispatch.phpsh#$t_deploy_typo3__phpbinary web/typo3/cli_dispatch.phpsh#g" ${RELEASES_DEVELOPMENT_PATH}/development/app/install_extensions.sh
                sleep 2

                #rename AdditionalConfiguration
                logMsg "rename AdditionalConfiguration"
                mv -v ${RELEASES_DEVELOPMENT_PATH}/development/app/web/typo3conf/_AdditionalConfiguration.php ${RELEASES_DEVELOPMENT_PATH}/development/app/web/typo3conf/AdditionalConfiguration.php
                sleep 1

                ;;
                *)
                errorMsg "Wrong argument [TODO]\n" \
                "'$1' => '$2' not valid\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;;
            esac
        ;;

        "development")
            case "$2" in
                "production")
                CASE=6

                logMsg "development > production $3";

                errorMsg "Bad news\n" \
                "Never do this again!\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit

                ;;
                *)
                errorMsg "Wrong argument [TODO]\n" \
                "'$1' => '$2' not valid\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;;
            esac
        ;;

        "staging")
            case "$2" in
                "development")
                CASE=7

                logMsg "staging > development $3";

                errorMsg "Bad news\n" \
                "This is not allowed!\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit

                ;;
                *)
                errorMsg "Wrong argument [TODO]\n" \
                "'$1' => '$2' not valid\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;;
            esac
        ;;


        ###################################
        ## Default
        ###################################
        *)
        errorMsg "Wrong argument [TODO]\n" \
        "Feel free to read the manual by executing 'make help'"
        exitMsg
        exit
        ;;
    esac


}

errorMsg() {
    echo -e "[$(tput setaf 3)$(tput setab 0)${BASH_LINENO}$(tput sgr 0)]\n" \
    $(tput setaf 1)$(tput setab 0)"\n"\
    $(tput setaf 1)$(tput setab 0)$*$(tput sgr 0)
}

checkMsg() {
    echo -e "" \
    "$(tput setaf 2)[x] $*$(tput sgr 0)"
}
checkWarningMsg() {
    echo -e "" \
    "$(tput setaf 3)[!] $*$(tput sgr 0)"
}
checkNotMsg() {
    echo -e $(tput setaf 1)$(tput setab 0)"" \
    "$(tput setaf 1)$(tput setab 0)[-] $*$(tput sgr 0)"
}

runMsg() {
    echo -e "" \
    "[run] '$*' [$(tput setaf 3)$(tput setab 0)${BASH_LINENO}$(tput sgr 0)]"
}

logMsg() {
    echo -e "" \
    "$*"
}

infoMsg() {
    echo -e "" \
    "\n" \
    "$*"
}

exitMsg() {
    errorMsg "[EXIT]\n" \
    "Good by'"

    cd t_deploy
}

warningMsg() {
    echo -e "" \
    "$(tput setaf 3)$(tput setab 0)$*$(tput sgr 0)"
}

infoMsg "Hello!"

##
## important
##
cd ..


##
## e.g. development staging 0.1
## e.g. staging production 0.1
## e.g. production development development
##
githelper $1 $2 $3