#!/bin/bash

##
## Variables
##
CONFIG_PATH="config/deploy"
RELEASES_PATH="releases"
RELEASES_DEVELOPMENT_PATH="${RELEASES_PATH}/development"
RELEASES_STAGING_PATH="${RELEASES_PATH}/staging"
RELEASES_PRODUCTION_PATH="${RELEASES_PATH}/production"
INSTALLER_PATH="t_installer"
TIME=$(date +"%Y-%m-%d_%H-%M-%S")
TRASH="trash/${TIME}"
#SQLDUMP_FOLDER="sqldump"
SQLDUMP_PATH="migrations/db"

##
## *.ini contains source-database credentials
##
SOURCE_INI=""

##
## *.ini to override in deployment 
## containes target-database credentials
##
TARGET_INI=""

##
## Generated file
##
GENERATED_FILE=""

##
## ignore tables
##
IGNORE_TABLES=""
 
db () {
    case "$1" in
        "development")
            case "$2" in
                "staging")

                ##
                ## there must be exactly one staging version
                ##
                COUNT_DIRECTORIES_IN_GIVEN_PATH=$(find ${RELEASES_STAGING_PATH} -mindepth 1 -maxdepth 1 -type d | wc -l)
                if [ ${COUNT_DIRECTORIES_IN_GIVEN_PATH} = 1 ]; 
                    then
                        ##
                        ## version must be equal to $3
                        ##
                        FIRST_DIRECTORY_IN_GIVEN_PATH=$(find ${RELEASES_STAGING_PATH} -type d | sort | tail -n +2 | head -1 | sed 's#.*/##')
                        if [ ! "${FIRST_DIRECTORY_IN_GIVEN_PATH}" = "${3}" ]; 
                            then
                                errorMsg "First directory in '${RELEASES_STAGING_PATH}' is '${FIRST_DIRECTORY_IN_GIVEN_PATH}'.\n" \
                                "${RELEASES_STAGING_PATH}/${LAST_DIRECTORY_IN_GIVEN_PATH} != ${RELEASES_STAGING_PATH}/${3}"
                                exitMsg;
                                exit; 
                        fi
                    else
                        errorMsg "Count directories in '${RELEASES_STAGING_PATH}' = ${COUNT_DIRECTORIES_IN_GIVEN_PATH} != 1."
                        exitMsg;
                        exit;
                fi

                ##
                ## check wether master branch exists in local INSTALLER 
                ##
                BRANCH=$(git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH branch --list "${3}")
                if [ "$BRANCH" = "" ]
                    then
                        checkNotMsg "Target branch '${3}' not merged and checked out in local INSTALLER."
                        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n" \
                        "Please fetch && merge && checkout branch '${3}' in local INSTALLER first. \n" \
                        "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
                        exitMsg
                        exit                    
                fi

                ##
                ## replace placeholders
                ##
                replacePlaceholders $1 $2 $3

                ;;
                *)
                errorMsg "Wrong argument\n" \
                "'$1' => '$2' not valid\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;;
            esac
        ;;

        "staging")
            case "$2" in
                "production")

                ##
                ## highest version must be equal to $3
                ##
                LAST_DIRECTORY_IN_GIVEN_PATH=$(find ${RELEASES_PRODUCTION_PATH} -type d | sort --reverse | head -1 | sed 's#.*/##')
                if [ ! "${LAST_DIRECTORY_IN_GIVEN_PATH}" = "${3}" ]; then
                    errorMsg "Last directory in '${RELEASES_PRODUCTION_PATH}' is '${LAST_DIRECTORY_IN_GIVEN_PATH}'.\n" \
                    "${RELEASES_PRODUCTION_PATH}/${LAST_DIRECTORY_IN_GIVEN_PATH} != ${RELEASES_PRODUCTION_PATH}/${3}"
                    exitMsg;
                    exit; 
                fi

                ##
                ## t_installer/.git must be present
                ##
                if [ ! -d ${INSTALLER_PATH}/.git ]; then
                    errorMsg "Directory '${INSTALLER_PATH}/.git' does not exist\n" \
                    "Feel free to read the manual by executing 'make help'"
                    exitMsg
                    exit
                fi

                ##
                ## check wether master branch exists in local INSTALLER 
                ##
                BRANCH=$(git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH branch --list "master")
                if [ "$BRANCH" = "" ]
                    then
                        checkNotMsg "Target branch 'master' does not exist in local INSTALLER."
                        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n" \
                        "Please fetch && merge && checkout target branch 'master' in local INSTALLER first. \n" \
                        "Branch from current development branch. \n" \
                        "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
                        exitMsg
                        exit
                    else 
                    ##
                    ## check wether tag ${3} exists in local INSTALLER 
                    ##
                    TAG=$(git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH tag --list "${3}")
                    if [ "$TAG" = "" ]
                    then
                        checkNotMsg "Tag '${3}' does not exist in 'master' in local INSTALLER."
                        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n" \
                        "Please fetch && merge && checkout tag '${3}' in local INSTALLER first. \n" \
                        "Do _NOT_ just create tag '${3}' in local INSTALLER. \n" \
                        "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
                        exitMsg
                        exit
                    fi
                fi
                
                ##
                ## replace placeholders
                ##
                replacePlaceholders $1 $2 $3

                ;;
                *)
                errorMsg "Wrong argument\n" \
                "'$1' => '$2' not valid\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;; 
            esac
        ;;

        "production")
            case "$2" in
                "development")
               
                ##
                ## replace placeholders
                ##
                replacePlaceholders $1 $2 $3

                ;;

                *)
                errorMsg "Wrong argument\n" \
                "'$1' => '$2' not valid\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;; 
            esac
        ;;

        ###################################
        ## Default
        ###################################
        *)
        errorMsg "Wrong argument [TODO]\n" \
        "Feel free to read the manual by executing 'make help'"
        exitMsg
        exit
        ;;
    esac

    
}

replacePlaceholders() {
    cp t_deploy/example.export.import.sh ${SQLDUMP_PATH}/${GENERATED_FILE}

    ##
    ## include source.ini variables
    ##
    . ${CONFIG_PATH}/${SOURCE_INI}

    SOURCE_DATABASE="$database"
    SOURCE_HOST="$host"
    SOURCE_PASSWORD="$password"
    SOURCE_PORT="$port"
    SOURCE_USERNAME="$username"

    ##
    ## get --ignore-table(s) if 
    ##
    if [ "${2}" = "production" ]; then ignoreTables ${SOURCE_DATABASE}; fi

    sed -ie "s/%%%SOURCE_USERNAME%%%/${SOURCE_USERNAME}/g" ${SQLDUMP_PATH}/${GENERATED_FILE}
    sed -ie "s/%%%SOURCE_PASSWORD%%%/${SOURCE_PASSWORD}/g" ${SQLDUMP_PATH}/${GENERATED_FILE}
    sed -ie "s/%%%SOURCE_HOST%%%/${SOURCE_HOST}/g" ${SQLDUMP_PATH}/${GENERATED_FILE}
    sed -ie "s/%%%SOURCE_DATABASE%%%/${SOURCE_DATABASE}/g" ${SQLDUMP_PATH}/${GENERATED_FILE}
    sed -ie "s/%%%IGNORE_TABLES%%%/${IGNORE_TABLES}/g" ${SQLDUMP_PATH}/${GENERATED_FILE}
    
    ##
    ## include target.ini variables
    ##
    . ${CONFIG_PATH}/${TARGET_INI}

    TARGET_DATABASE="$database"
    TARGET_HOST="$host"
    TARGET_PASSWORD="$password"
    TARGET_PORT="$port"
    TARGET_USERNAME="$username"

    sed -ie "s/%%%TARGET_USERNAME%%%/${TARGET_USERNAME}/g" ${SQLDUMP_PATH}/${GENERATED_FILE}
    sed -ie "s/%%%TARGET_PASSWORD%%%/${TARGET_PASSWORD}/g" ${SQLDUMP_PATH}/${GENERATED_FILE}
    sed -ie "s/%%%TARGET_HOST%%%/${TARGET_HOST}/g" ${SQLDUMP_PATH}/${GENERATED_FILE}
    sed -ie "s/%%%TARGET_DATABASE%%%/${TARGET_DATABASE}/g" ${SQLDUMP_PATH}/${GENERATED_FILE}

    ##
    ## replace TIME
    ##
    sed -ie "s/%%%TIME%%%/${TIME}/g" ${SQLDUMP_PATH}/${GENERATED_FILE}

    ##
    ## echo result
    ##
    infoMsg "Genereated '${GENERATED_FILE}' in '${SQLDUMP_PATH}' \n" \
    "Please run '$(tput setaf 3)$(tput setab 0)sh ../${SQLDUMP_PATH}/${GENERATED_FILE}$(tput sgr 0)' to import/export databases \n" \
    "or '$(tput setaf 3)$(tput setab 0)cat ../${SQLDUMP_PATH}/${GENERATED_FILE}$(tput sgr 0)' to view the generated file."

    infoMsg "Done :)"
}

ignoreTables() {
    . t_deploy/db-helper.ini

    IGNORE_TABLES=""
    for TABLE in "${production_ignore_tables[@]}"
    do :
        IGNORE_TABLES+=" --ignore-table=${SOURCE_DATABASE}.${TABLE}"
    done
}

errorMsg() {
    echo -e "" \
    "$(tput setaf 1)$(tput setab 0)"
    warningMsg "[${BASH_LINENO}]"
    echo -e $(tput sgr 0) $(tput setaf 1)$(tput setab 0)$*$(tput sgr 0)
}

checkMsg() {
    echo -e "" \
    "$(tput setaf 2)[x] $*$(tput sgr 0)"
}
checkWarningMsg() {
    echo -e "" \
    "$(tput setaf 3)[!] $*$(tput sgr 0)"
}
checkNotMsg() {
    echo -e $(tput setaf 1)$(tput setab 0)"" \
    "$(tput setaf 1)$(tput setab 0)[-] $*$(tput sgr 0)"
}

runMsg() {
    echo -e "" \
    "[run] '$*' [$(tput setaf 3)$(tput setab 0)${BASH_LINENO}$(tput sgr 0)]"
}

logMsg() {
    echo -e "" \
    "$*"
}

infoMsg() {
    echo -e "" \
    "\n" \
    "$*"
}

exitMsg() {
    errorMsg "[EXIT]\n" \
    "Good by'"

    cd t_deploy
}

warningMsg() {
    echo -e "" \
    "$(tput setaf 3)$(tput setab 0)$*$(tput sgr 0)"
}

infoMsg "Hello!"

if [ "$#" -lt 1 ] || [ "$#" -lt 2 ] || [ "$#" -lt 3 ]; then
    errorMsg "Missing argument\n" \
    "Feel free to read the manual by executing 'make help'"
    exit
fi

SOURCE_INI="${1}.ini"
##
## production.ini is in use already, so we have to use our production1.ini temporary.
## we mv production.ini and production1.ini later (but not in this script)! 
##
if [ "${2}" = "production" ]; then TARGET_INI="${2}1.ini"; else TARGET_INI="${2}.ini"; fi
GENERATED_FILE="${1}__${2}__${3}__${TIME}.sh"

##
## important
##
cd ..

##
## run :)
## e.g. development staging 0.1
## e.g. staging production 0.1
## e.g. production development development
##
db $1 $2 $3