ARGS = $(filter-out $@,$(MAKECMDGOALS))
MAKEFLAGS += --silent

list:
	sh -c "echo; $(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\/\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'Makefile'| sort"

#############################
# Help
#############################

help:
	bash manual.sh $(ARGS)

###############################
# Deploy
###############################

deploy:
	bash echo "deprecated. do not use anymore."
	# deploy.sh $(ARGS)
	# -x for debugging

###############################
# db-helper
###############################

db-helper:
	bash 2-db-helper.sh $(ARGS)
	# -x for debugging

#############################
# git-helper
#############################

git-helper:
	bash 1-git-helper.sh $(ARGS)

#############################
# update-helper
#############################

update-helper:
	bash 3-update-helper.sh $(ARGS)

#############################
# Argument fix workaround
#############################
%:
	@: