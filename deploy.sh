#!/bin/bash

##
## Variables
##


CONFIG_PATH="config/deploy"
RELEASES_PATH="releases"
RELEASES_DEVELOPMENT_PATH="${RELEASES_PATH}/development"
RELEASES_STAGING_PATH="${RELEASES_PATH}/staging"
RELEASES_PRODUCTION_PATH="${RELEASES_PATH}/production"
INSTALLER_PATH="t_installer"
TRASH="trash/"$(date +"%Y-%m-%d_%H-%M-%S")
SQLDUMP_FOLDER="sqldump"

##
## *.ini contains source-database credentials
##
SOURCE_INI=""

##
## *.ini to override in deployment 
## containes target-database credentials
##
TARGET_INI=""

##
## empty as long as there are enough free *.ini's to override
##
OLDEST_EXISTING_PRODUCTION_VERSION=""

##
## empty if there is no current stagin version, else remove in the process
##
CURRENT_STAGING_VERSION=""

deploy() {
    
    infoMsg "To do our job, we need some neat packages."
    warningMsg "git find sort tail head sed du cp mv pwd dirname";

    runMsg "Change directory 'cd..'"
    cd ..

    # infoMsg "Test required packages:"

    # REQUIRE_HEAD=$(which head)
    # if [ "$REQUIRE_HEAD" = "" ];
    # then
    #     errorMsg "'head' missing\n" \
    #     "Please install package 'head'\n" \
    #     "Feel free to read the manual by executing 'make help'"
    #     exitMsg
    #     exit
    # else
    #     checkMsg "head"
    #     infoMsg "tested version:   head (GNU coreutils) 8.23\n" \
    #     "installed verion: $(head --version | head -n 1)"
    # fi

    # compareVersions "8.23" "head";
    # compareVersions "2.16" "grep";
    # compareVersions "8.23" "du";
    # compareVersions "8.23" "cp";
    # compareVersions "4.4.2" "find";

    infoMsg "$DRY"

    infoMsg "So, you want to deploy $(tput setaf 3)$(tput setab 0)'$1/$2'$(tput sgr 0) => $(tput setaf 3)$(tput setab 0)'$3/$4'$(tput sgr 0)"

    case "$1" in
        "development")
            case "$3" in
                "staging")
                infoMsg "Ok, seems valid. \n" \
                "Let's take a closer look.'"
                ;;
                *)
                errorMsg "Wrong argument [TODO]\n" \
                "Please do not deploy SOURCE '$1' => '$3'\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;;
            esac
        ;;

        "staging")
            case "$3" in
                "production")
                infoMsg "Ok, seems valid"
                ;;
                *)
                errorMsg "Wrong argument [TODO]\n" \
                "Please do not deploy SOURCE '$1' => '$3'\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;; 
            esac
        ;;

        ###################################
        ## Default
        ###################################
        *)
        errorMsg "Wrong argument [TODO]\n" \
        "Feel free to read the manual by executing 'make help'"
        exitMsg
        exit
        ;;
    esac

    infoMsg "Let's' take a look at the current project structure"
    runMsg "$ du -h -d 2 ${CONFIG_PATH}"
    runMsg "$ ls ${CONFIG_PATH}"
    runMsg "$ du -h -d 2 ${RELEASES_PATH}"
    infoMsg " + - - - - - - - - - - - - - - - - - - - - - - -  \n"
    du -h -d 2 ${CONFIG_PATH}
    ls ${CONFIG_PATH}
    du -h -d 2 ${RELEASES_PATH}
    infoMsg " + - - - - - - - - - - - - - - - - - - - - - - -  \n"

    ##
    ## Which *.ini should be overritten in the process?
    ##
    
    ## ../config d.ini exists?
    if [ -f "${CONFIG_PATH}/d.ini" ]; then
        TARGET_INI="d.ini"
    fi
    ## ../config c.ini exists?
    if [ -f "${CONFIG_PATH}/c.ini" ]; then
        TARGET_INI="c.ini"
    fi
    ## ../config/deploy b.ini exists?
    if [ -f "${CONFIG_PATH}/b.ini" ]; then
        TARGET_INI="b.ini"
    fi
    ## ../config b && c && d.ini does not exist?
    if [ "$TARGET_INI" = "" ]; then
        ##
        ## if not empty => mv directory to trash in the process
        ##
        OLDEST_EXISTING_PRODUCTION_VERSION=$(find $RELEASES_PRODUCTION_PATH -type d | sort | tail -n +2 | head -1 | sed 's#.*/##')
        if [ -f "${CONFIG_PATH}/${OLDEST_EXISTING_PRODUCTION_VERSION}".ini ]; 
        then
            TARGET_INI="$OLDEST_EXISTING_PRODUCTION_VERSION.ini"
            checkMsg "Override '$TARGET_INI'"
        else
            errorMsg "Bad news\n" \
            "We have no 'b.ini', 'c.ini' or 'd.ini'!\n" \
            "And we have no older 'production.ini' either!\n" \
            "This shoud never ever happen!\n" \
            "Feel free to read the manual by executing 'make help'"
            exitMsg
            exit
        fi
    fi

    ##
    ## Given Target (directory) and corresponding target.ini must NOT be present
    ## e.g. /releases/staging/0.3 and config/deploy/0.3.ini
    ## or /releases/production/0.5 and config/deploy/0.5.ini
    ## 
    TARGET=$3
    TARGET_VERSION=$4

    runMsg "if [ -d \"${RELEASES_PATH}/${TARGET}/${TARGET_VERSION}\" ];"
    if [ -d "${RELEASES_PATH}/${TARGET}/${TARGET_VERSION}" ]; then
        errorMsg "Bad news\n" \
        "Directory (TARGET_VERSION) '${RELEASES_PATH}/${TARGET}/${TARGET_VERSION}' already exists\n" \
        "Ask yourself: Who the fuck staged this version (${TARGET_VERSION}) already?\n" \
        "Timo, is it you??\n" \
        "Feel free to read the manual by executing 'make help'"
        exitMsg
        exit
    fi
    checkMsg "Directory (TARGET_VERSION) '${RELEASES_PATH}/${TARGET}/${TARGET_VERSION}' does not exist"

    ##
    ## Given Source (directory) and corresponding Source.ini must be present
    ## e.g. /releases/development/development and config/deploy/development.ini
    ## or /releases/staging/source and config/deploy/source.ini
    ##  
    SOURCE=$1
    SOURCE_VERSION=$2

    ##
    ## Get Source.ini 
    ##
    SOURCE_INI=$(find ${CONFIG_PATH} -type f -name ${SOURCE_VERSION}.ini | sed 's#.*/##')
    runMsg "find ${CONFIG_PATH} -type f -name ${SOURCE_VERSION}.ini | sed 's#.*/##'"
    ## ../config *.ini exists?
    if [ ! -f "${CONFIG_PATH}/$SOURCE_INI" ]; then
        errorMsg "File (SOURCE_INI) '$CONFIG_PATH/$SOURCE_VERSION.ini' does not exist\n" \
        "Feel free to read the manual by executing 'make help'"
        exitMsg
        exit
    fi
    checkMsg "File (SOURCE_INI) '$SOURCE_INI' exists"

    runMsg "if [ ! -d \"${RELEASES_PATH}/${SOURCE}/${SOURCE_VERSION}\" ];"
    if [ ! -d "${RELEASES_PATH}/${SOURCE}/${SOURCE_VERSION}" ]; then
        errorMsg "Directory (SOURCE_VERSION) '${RELEASES_PATH}/${SOURCE}/${SOURCE_VERSION}' does not exist\n" \
        "Feel free to read the manual by executing 'make help'"
        exitMsg
        exit
    fi
    checkMsg "Directory (SOURCE_VERSION) '${RELEASES_PATH}/${SOURCE}/${SOURCE_VERSION}' exists"

    ##
    ## t_installer/.git must be present
    ##
    runMsg "if [ ! -d ${INSTALLER_PATH}/.git ];"
    if [ ! -d ${INSTALLER_PATH}/.git ]; then
        errorMsg "Directory '${INSTALLER_PATH}/.git' does not exist\n" \
        "Feel free to read the manual by executing 'make help'"
        exitMsg
        exit
    fi
    checkMsg "Directory '${INSTALLER_PATH}/.git' present"

    ##
    ## fetch latest t_installer from .git
    ##
    runMsg "git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} fetch"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
    else
        git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} fetch
        sleep 2
    fi

    ##
    ## merge latest t_installer from .git
    ##
    runMsg "git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} merge"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
    else
        git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} merge
        sleep 2
    fi

    ##
    ## check wether target branch exists in INSTALLER 
    ##
    runMsg "git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH branch -a | grep \"remotes/origin/$TARGET_VERSION\""
    BRANCH=$(git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH branch -a | grep "remotes/origin/$TARGET_VERSION")
    infoMsg "BRANCH: $BRANCH"
    if [ "$BRANCH" = "" ]
    then
        checkNotMsg "Target branch '$TARGET_VERSION' does not exist."
        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - \n" \
        "Please create target branch '$TARGET_VERSION' first. \n" \
        "Branch from current development branch. \n" \
        "+ - - - - - - - - - - - - - - - - - - - - - - -\n"
        exitMsg
        exit
    fi

    ##
    ## Create TRASH directory
    ##
    runMsg "mkdir ${TRASH}"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN"; else mkdir ${TRASH}; fi

    ##
    ## Magic below ;)
    ##
    warningMsg "\n:::::::::::::::::\n:: Magic below :: \n:::::::::::::::::"

    case "$1" in
        "development")
            case "$3" in
                "staging")
                developmentToStaging $SOURCE $SOURCE_VERSION $SOURCE_INI $TARGET $TARGET_VERSION $TARGET_INI $OLDEST_EXISTING_PRODUCTION_VERSION $DRY
                ;;
                *)
                errorMsg "Wrong argument\n" \
                "Please do not deploy from '$1' => '$3'\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;;
            esac
        ;;

        "staging")
            case "$3" in
                "production")

                ## check wether target TAG exists in INSTALLER 
                logMsg "git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH tag --list $TARGET_VERSION"
                TAG=$(git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH tag --list $TARGET_VERSION)
                if [ "$TAG" = "" ]
                then
                    echo "Tag name '$TARGET_VERSION' does not exist in 'master'."
                    exitMsg
                    exit
                fi
                stagingToProduction $SOURCE $SOURCE_VERSION $SOURCE_INI $TARGET $TARGET_VERSION $TARGET_INI $OLDEST_EXISTING_PRODUCTION_VERSION $DRY
                ;;
                *)
                errorMsg "Wrong argument\n" \
                "Please do not deploy SOURCE '$1' => '$3'\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;;
            esac
        ;;

        ###################################
        ## Default
        ###################################
        *)
        errorMsg "Wrong argument [TODO]\n" \
        "Feel free to read the manual by executing 'make help'"
        exitMsg
        exit
        ;;

    esac    

}

developmentToStaging() {
    ## dev => staging :: cp && dump db && import db && mv ini 

    ##
    ## FIRST_DIRECTORY_IN_STAGING_PATH is NOT an empty string?
    ## There is a staged version!
    ##
    runMsg "find ${RELEASES_STAGING_PATH} -type d | sort | tail -n +2 | head -1 | sed 's#.*/##'"
    FIRST_DIRECTORY_IN_STAGING_PATH=$(find ${RELEASES_STAGING_PATH} -type d | sort | tail -n +2 | head -1 | sed 's#.*/##')
    if [ -n "$FIRST_DIRECTORY_IN_STAGING_PATH" ]; then
        ##
        ## There should be a corresponding *.ini file!
        ##
        if [ -f "${CONFIG_PATH}/${FIRST_DIRECTORY_IN_STAGING_PATH}.ini" ]; 
            then
                EXISTING_STAGING_INI="${FIRST_DIRECTORY_IN_STAGING_PATH}.ini"
                checkWarningMsg "Staging not empty!"
                checkMsg "Override '${TARGET_INI}', use '${EXISTING_STAGING_INI}'"
                TARGET_INI="${EXISTING_STAGING_INI}"
                
                checkWarningMsg "$RELEASES_STAGING_PATH/$FIRST_DIRECTORY_IN_STAGING_PATH needs to be removed"
                runMsg "mv ${RELEASES_STAGING_PATH}/${FIRST_DIRECTORY_IN_STAGING_PATH} ${TRASH}"
                if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
                else
                    mv ${RELEASES_STAGING_PATH}/${FIRST_DIRECTORY_IN_STAGING_PATH} ${TRASH}
                fi
            else
            ##
            ## There is no corresponding *.ini file!
            ## This should never happen! Something went horribly wrong!
            ##
            errorMsg "Serious Error\n" \
            "There is a staged version '${FIRST_DIRECTORY_IN_STAGING_PATH}'\n" \
            "But there is no corresponding '${FIRST_DIRECTORY_IN_STAGING_PATH}.ini\n" \
            "This should never ever happen! Something went horribly wrong!\n" \
            "Feel free to read the manual by executing 'make help'"
            exitMsg
            exit
        fi
    fi

    ##
    ## rename target.ini
    ##
    runMsg "mv ${CONFIG_PATH}/${TARGET_INI} ${CONFIG_PATH}/${TARGET_VERSION}.ini"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
    else
        mv ${CONFIG_PATH}/${TARGET_INI} ${CONFIG_PATH}/${TARGET_VERSION}.ini
        TARGET_INI=${TARGET_VERSION}.ini
        sleep 1
    fi

    runMsg "cp -rp ${RELEASES_PATH}/${SOURCE}/${SOURCE_VERSION} ${RELEASES_PATH}/${TARGET}"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
    else
        cp -rp ${RELEASES_PATH}/${SOURCE}/${SOURCE_VERSION} ${RELEASES_PATH}/${TARGET}
        sleep 1
    fi

    runMsg "mv ${RELEASES_PATH}/${TARGET}/${SOURCE_VERSION} ${RELEASES_PATH}/${TARGET}/${TARGET_VERSION}"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN"; else
        mv ${RELEASES_PATH}/${TARGET}/${SOURCE_VERSION} ${RELEASES_PATH}/${TARGET}/${TARGET_VERSION}
    fi

    runMsg "cp -fp ${CONFIG_PATH}/${TARGET_INI} ${RELEASES_PATH}/${TARGET}/${TARGET_VERSION}/app/web/typo3conf/db.ini"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
    else
        cp -fp ${CONFIG_PATH}/${TARGET_INI} ${RELEASES_PATH}/${TARGET}/${TARGET_VERSION}/app/web/typo3conf/db.ini
        sleep 1
    fi

    runMsg "mv ${RELEASES_PATH}/${TARGET}/${TARGET_VERSION}/app/web/typo3temp/* ${TRASH}/typo3temp"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN"
    else
        mv ${RELEASES_PATH}/${TARGET}/${TARGET_VERSION}/app/web/typo3temp/* ${TRASH}/typo3temp
    fi

    infoMsg "Let's take a look at the resulting project structure \n" \
    " + - - - - - - - - - - - - - - - - - - - - - - -  \n"
    runMsg "$ du -h -d 2 ${CONFIG_PATH}"
    if [ "$DRY" = "dry" ]; then warningMsg " DRY RUN so nothing has been changed!"; fi
    du -h -d 2 ${CONFIG_PATH}

    runMsg "$ ls ${CONFIG_PATH}"
    if [ "$DRY" = "dry" ]; then warningMsg " DRY RUN so nothing has been changed!"; fi
    ls ${CONFIG_PATH}

    runMsg "$ du -h -d 2 ${RELEASES_PATH}" 
    if [ "$DRY" = "dry" ]; then warningMsg " DRY RUN so nothing has been changed!"; fi
    du -h -d 2 ${RELEASES_PATH}
    infoMsg " + - - - - - - - - - - - - - - - - - - - - - - -  \n"
    
    runMsg "git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} checkout ${TARGET_VERSION}"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
    else
        git --git-dir=${INSTALLER_PATH}/.git --work-tree=${INSTALLER_PATH} checkout ${TARGET_VERSION}
        sleep 1
    fi

    runMsg "if [ -f ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/composer.phar ];"
    if [ -f ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/composer.phar ]; then
        runMsg "mv ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/composer.phar ${TRASH}";
        if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
        else
            mv ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/composer.phar ${TRASH}
            sleep 1
        fi
    fi

    runMsg "curl -o ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/composer.phar https://getcomposer.org/composer.phar && sleep 2";
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
    else
        curl -o ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/composer.phar https://getcomposer.org/composer.phar
        sleep 4
    fi

    runMsg "cp -fp ${INSTALLER_PATH}/src/composer.json ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web"
    runMsg "chmod 755 ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/composer.json"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
    else
        cp -fp ${INSTALLER_PATH}/src/composer.json ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web
        sleep 1
        #chmod -R 755 ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/composer.json
     fi

    runMsg "cp -fp ${INSTALLER_PATH}/src/install_extensions.sh ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
    else
        cp -fp ${INSTALLER_PATH}/src/install_extensions.sh ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web
        sleep 1
        #chmod -R755 ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/install_extensions.sh
    fi

    #include deploy.ini variables
    runMsg ". t_deploy/deploy.ini"
    . t_deploy/deploy.ini

    if [ "$t_deploy_typo3__phpbinary" = "" ]; then
        checkNotMsg "PHP binary path is empty"
        exitMsg
        exit
    fi
    checkMsg "PHP binary path set TARGET '$t_deploy_typo3__phpbinary'"

    runMsg "${t_deploy_typo3__phpbinary} ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/composer.phar update"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
    else
        ${t_deploy_typo3__phpbinary} ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/composer.phar update
    fi

    runMsg "sed -ie \"s#php web/typo3/cli_dispatch.phpsh#$t_deploy_typo3__phpbinary web/typo3/cli_dispatch.phpsh#g\" ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/install_extensions.sh"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN"; else
        sed -ie "s#php web/typo3/cli_dispatch.phpsh#$t_deploy_typo3__phpbinary web/typo3/cli_dispatch.phpsh#g" ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/install_extensions.sh
    fi
    
    runMsg "sh ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/install_extensions.sh"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
    else
        sh ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web/install_extensions.sh
    fi

    runMsg "if [ -L ${TARGET} ];"
    if [ -L ${TARGET} ]; then
        runMsg "mv ${TARGET} ${TRASH}"
        if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
        else
            mv ${TARGET} ${TRASH}
        fi
    fi

    #add symlink
    runMsg "ln -s ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web ${TARGET}"
    if [ "$DRY" = "dry" ]; then warningMsg "DRY RUN";
    else
        ln -s ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/app/web ${TARGET}
    fi

    #include source.ini variables
    runMsg ". ${CONFIG_PATH}/${SOURCE_INI}"
    . ${CONFIG_PATH}/${SOURCE_INI}

    #prefix=a
    #a_database=xxxxxxxxxx
    #a_extTablesDefinitionScript=extTables.php
    #a_host=127.0.0.3
    #a_password="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"nan o
    #a_port=3306
    #a_username=xxxxxxxxxx
    SOURCE_PREFIX=$prefix
    case "$SOURCE_PREFIX" in
        "local")
            checkMsg "DB ini prefix (SOURCE): $SOURCE_PREFIX"
            SOURCE_DATABASE="$local_database"
            SOURCE_HOST="$local_host"
            SOURCE_PASSWORD="$local_password"
            SOURCE_PORT="$local_port"
            SOURCE_USERNAME="$local_username"
        ;;
        "hotfix")
            checkMsg "DB ini prefix (SOURCE): $SOURCE_PREFIX"
            SOURCE_DATABASE="$hotfix_database"
            SOURCE_HOST="$hotfix_host"
            SOURCE_PASSWORD="$hotfix_password"
            SOURCE_PORT="$hotfix_port"
            SOURCE_USERNAME="$hotfix_username"
        ;;
        "a")
            checkMsg "DB ini prefix (SOURCE): $SOURCE_PREFIX"
            SOURCE_DATABASE="$a_database"
            SOURCE_HOST="$a_host"
            SOURCE_PASSWORD="$a_password"
            SOURCE_PORT="$a_port"
            SOURCE_USERNAME="$a_username"
        ;;
        "b")
            checkMsg "DB ini prefix (SOURCE): $SOURCE_PREFIX"
            SOURCE_DATABASE="$b_database"
            SOURCE_HOST="$b_host"
            SOURCE_PASSWORD="$b_password"
            SOURCE_PORT="$b_port"
            SOURCE_USERNAME="$b_username"
        ;;
        "c")
            checkMsg "DB ini prefix (SOURCE): $SOURCE_PREFIX"
            SOURCE_DATABASE="$c_database"
            SOURCE_HOST="$c_host"
            SOURCE_PASSWORD="$c_password"
            SOURCE_PORT="$c_port"
            SOURCE_USERNAME="$c_username"
        ;;
        "d")
            checkMsg "DB ini prefix (SOURCE): $SOURCE_PREFIX"
            SOURCE_DATABASE="$d_database"
            SOURCE_HOST="$d_host"
            SOURCE_PASSWORD="$d_password"
            SOURCE_PORT="$d_port"
            SOURCE_USERNAME="$d_username"
        ;;
        *)
        checkNotMsg "Wrong configuration in '$CONFIG_PATH/$SOURCE_INI'."
        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - \n" \
        "No valid prefix! \n" \
        "Please check '$CONFIG_PATH/$SOURCE_INI'. \n" \
        "+ - - - - - - - - - - - - - - - - - - - - - - -\n"
        exitMsg
        exit
        ;;
    esac

    if [ "$SOURCE_PREFIX" = "" ] || [ "$SOURCE_DATABASE" = "" ] || [ "$SOURCE_HOST" = "" ] || [ "$SOURCE_PASSWORD" = "" ] || [ "$SOURCE_PORT" = "" ] || [ "$SOURCE_USERNAME" = "" ]; then
        checkNotMsg "Wrong configuration in '$CONFIG_PATH/$SOURCE_INI'."
        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - \n" \
        "Please edit '$CONFIG_PATH/$SOURCE_INI' first. \n" \
        "+ - - - - - - - - - - - - - - - - - - - - - - -\n"
        exitMsg
        exit
    fi

    runMsg ". ${CONFIG_PATH}/${TARGET_INI}"
    . ${CONFIG_PATH}/${TARGET_INI}

    TARGET_PREFIX=$prefix
    case "$TARGET_PREFIX" in
        "local")
            checkMsg "DB ini prefix (TARGET): $TARGET_PREFIX"
            TARGET_DATABASE="$local_database"
            TARGET_HOST="$local_host"
            TARGET_PASSWORD="$local_password"
            TARGET_PORT="$local_port"
            TARGET_USERNAME="$local_username"
        ;;
        "hotfix")
            checkMsg "DB ini prefix (TARGET): $TARGET_PREFIX"
            TARGET_DATABASE="$hotfix_database"
            TARGET_HOST="$hotfix_host"
            TARGET_PASSWORD="$hotfix_password"
            TARGET_PORT="$hotfix_port"
            TARGET_USERNAME="$hotfix_username"
        ;;
        "a")
            checkMsg "DB ini prefix (TARGET): $TARGET_PREFIX"
            TARGET_DATABASE="$a_database"
            TARGET_HOST="$a_host"
            TARGET_PASSWORD="$a_password"
            TARGET_PORT="$a_port"
            TARGET_USERNAME="$a_username"
        ;;
        "b")
            checkMsg "DB ini prefix (TARGET): $TARGET_PREFIX"
            TARGET_DATABASE="$b_database"
            TARGET_HOST="$b_host"
            TARGET_PASSWORD="$b_password"
            TARGET_PORT="$b_port"
            TARGET_USERNAME="$b_username"
        ;;
        "c")
            checkMsg "DB ini prefix (TARGET): $TARGET_PREFIX"
            TARGET_DATABASE="$c_database"
            TARGET_HOST="$c_host"
            TARGET_PASSWORD="$c_password"
            TARGET_PORT="$c_port"
            TARGET_USERNAME="$c_username"
        ;;
        "d")
            checkMsg "DB ini prefix (TARGET): $TARGET_PREFIX"
            TARGET_DATABASE="$d_database"
            TARGET_HOST="$d_host"
            TARGET_PASSWORD="$d_password"
            TARGET_PORT="$d_port"
            TARGET_USERNAME="$d_username"
        ;;
        *)
        checkNotMsg "Wrong configuration in '${CONFIG_PATH}/${TARGET_INI}'."
        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - \n" \
        "No valid prefix! \n" \
        "Please check '${CONFIG_PATH}/${TARGET_INI}'. \n" \
        "+ - - - - - - - - - - - - - - - - - - - - - - -\n"
        exitMsg
        exit
        ;;
    esac

    if [ "${TARGET_PREFIX}" = "" ] || [ "${TARGET_DATABASE}" = "" ] || [ "${TARGET_HOST}" = "" ] || [ "${TARGET_PASSWORD}" = "" ] || [ "${TARGET_PORT}" = "" ] || [ "${TARGET_USERNAME}" = "" ]; then
        checkNotMsg "Wrong configuration in '${CONFIG_PATH}/${TARGET_INI}'."
        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - \n" \
        "Please edit '${CONFIG_PATH}/${TARGET_INI}' first. \n" \
        "+ - - - - - - - - - - - - - - - - - - - - - - -\n"
        exitMsg
        exit
    fi

    if [ "${SOURCE_PREFIX}" = "${TARGET_PREFIX}" ]; then
        checkNotMsg "Error: Prefix (SOURCE) and (TARGET) must be different!"
        exitMsg
        exit
    fi

    TIME=$(date +"%Y-%m-%d_%H-%M-%S")    
    runMsg "mysqldump --defaults-extra-file=<(printf \"[client]\\nuser = %s\\npassword = %s\\nhost = %s\" \"${SOURCE_USERNAME}\" \"'${SOURCE_PASSWORD}'\" \"${SOURCE_HOST}\") -v ${SOURCE_DATABASE} --single-transaction --skip-add-locks --no-autocommit > ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/${SQLDUMP_FOLDER}/mysqldump_${SOURCE_DATABASE}_${TIME}.sql"
    #runMsg "mysqldump --defaults-extra-file=${CONFIG_PATH}/b.cnf -v ${SOURCE_DATABASE} --single-transaction --skip-add-locks --no-autocommit > ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/${SQLDUMP_FOLDER}/mysqldump_${SOURCE_DATABASE}_${TIME}.sql"
    if [ "$DRY" = "dry" ]; 
        then 
            warningMsg "DRY RUN" 
        else 
            runMsg "if [ ! -d ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/${SQLDUMP_FOLDER} ];"
            if [ ! -d ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/${SQLDUMP_FOLDER} ]; then
                runMsg "mkdir ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/${SQLDUMP_FOLDER}"
                    if [ "$DRY" = "dry" ]; 
                        then 
                            warningMsg "DRY RUN"
                        else 
                            mkdir ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/${SQLDUMP_FOLDER}
                            sleep 2
                    fi
            fi
            checkMsg "Directory '${RELEASES_STAGING_PATH}/${TARGET_VERSION}/${SQLDUMP_FOLDER}' present"
            #mysqldump -h$SOURCE_HOST $SOURCE_DATABASE --single-transaction --skip-add-locks --no-autocommit -u$SOURCE_USERNAME --password=$SOURCE_PASSWORD > $RELEASES_STAGING_PATH/$TARGET_VERSION/$SQLDUMP_FOLDER/mysqldump_$SOURCE_DATABASE_$TIME.sql
            mysqldump --defaults-extra-file=<(printf "[client]\nuser = %s\npassword = %s\nhost = %s" "${SOURCE_USERNAME}" "'${SOURCE_PASSWORD}'" "${SOURCE_HOST}") -v ${SOURCE_DATABASE} --single-transaction --skip-add-locks --no-autocommit > ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/${SQLDUMP_FOLDER}/mysqldump_${SOURCE_DATABASE}_${TIME}.sql
            sleep 2

            #mysqldump --defaults-extra-file=${CONFIG_PATH}/b.cnf -v ${SOURCE_DATABASE} --single-transaction --skip-add-locks --no-autocommit > ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/${SQLDUMP_FOLDER}/mysqldump_${SOURCE_DATABASE}_${TIME}.sql
            #sleep 2 
    fi

    runMsg "nohup mysql --defaults-extra-file=<(printf \"[client]\\nuser = %s\\npassword = %s\\nhost = %s\" \"${TARGET_USERNAME}\" \"'${TARGET_PASSWORD}'\" \"${TARGET_HOST}\") ${TARGET_DATABASE} --default-character-set=utf8 < ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/${SQLDUMP_FOLDER}/mysqldump_${SOURCE_DATABASE}_${TIME}.sql 2000 1>${TRASH}/stdout.txt 2>${TRASH}/stderr.txt &"
    if [ "$DRY" = "dry" ]; 
        then 
            warningMsg "DRY RUN" 
        else 
            nohup mysql --defaults-extra-file=<(printf "[client]\nuser = %s\npassword = %s\nhost = %s" "${TARGET_USERNAME}" "'${TARGET_PASSWORD}'" "${TARGET_HOST}") ${TARGET_DATABASE} --default-character-set=utf8 1>${TRASH}/stdout.txt 2>${TRASH}/stderr.txt < ${RELEASES_STAGING_PATH}/${TARGET_VERSION}/${SQLDUMP_FOLDER}/mysqldump_${SOURCE_DATABASE}_${TIME}.sql &

            sleep 2

            #id of last backgorund task
            echo "[$!]"
    fi

    warningMsg "\n:::::::::::::::::::::::::::\n:: Congrats. We're done. ::\n:::::::::::::::::::::::::::\n" \
    "\n" \
    "[TODO] Be patient. Visit deployed site as soon as database import (with process id [$!]) has been finished :)\n" \
    "[TODO] Please remove  '${TRASH}' if deployment has been successful!"

    cd t_deploy
}
stagingToProduction() {
    ## staging => prod :: mv staging && mv ini
    echo "$RELEASES_DEVELOPMENT_PATH"
}

compareVersions() {
    VT="$1"; APP="$2"

    VT=$(echo $1 | grep --only-matching --basic-regexp "\\d+.\\d+.\\d+");
    if [ "$VT" = "" ]; 
        then 
            VT=$(echo $1 | grep --only-matching ---basic-regexp "\\d+\.\\d+");
            VT_0=$(echo $VT | grep --only-matching --basic-regexp "^\\d+");
            VT_1=$(echo $VT | grep --only-matching --basic-regexp "\\d+$");
            VT_2="0"; 
        else 
            VT_0=$(echo $VT | grep --only-matching --perl-regexp "^\\d+");
            VT_1=$(echo $(echo $VT | grep --only-matching --perl-regexp "^\\d+\.\\d+") | grep --only-matching --perl-regexp "\\d+$");
            VT_2=$(echo $VT | grep --only-matching --perl-regexp "\\d+$");
    fi
    

    VI=$($APP --version | head -n 1 | grep --only-matching --perl-regexp "\\d+.\\d+.\\d+");
    if [ "$VI" = "" ]; 
        then 
            VI=$($APP --version | head -n 1 | grep --only-matching --perl-regexp "\\d+\.\\d+");
            VI_0=$(echo $VI | grep --only-matching --perl-regexp "^\\d+");
            VI_1=$(echo $VI | grep --only-matching --perl-regexp "\\d+$");
            VI_2="9999"; 
        else 
            VI_0=$(echo $VI | grep --only-matching --perl-regexp "^\\d+");
            VI_1=$(echo $(echo $VI | grep --only-matching --perl-regexp "^\\d+\.\\d+") | grep --only-matching --perl-regexp "\\d+$");
            VI_2=$(echo $VI | grep --only-matching --perl-regexp "\\d+$");
    fi
    
    
    infoMsg "tested version:  ${VT}\n" \
    "installed verion: ${VI}"
    if [ "$VI" != "" ] && [ $VI_0 -gt $VT_0 ]; 
        then
            #infoMsg "1: $VI_0 -gt $VT_0";
            checkMsg "$2";
        else
            if [ "$VI" != "" ] && [ $VI_0 -ge $VT_0 ] && [ $VI_1 -gt $VT_1 ];
                then
                    #infoMsg "2: $VI_0 -ge $VT_0 && $VI_1 -gt $VT_1";
                    checkMsg "$2";
                else
                    if [ "$VI" != "" ] && [ $VI_0 -ge $VT_0 ] && [ $VI_1 -ge $VT_1 ] && [ $VI_2 -ge $VT_2 ];
                        then
                            checkMsg "$2";
                        else
                            #infoMsg "3"
                            checkNotMsg "$2"
                            exitMsg
                            exit
                    fi
            fi
    fi
}

errorMsg() {
    echo -e "[$(tput setaf 3)$(tput setab 0)${BASH_LINENO}$(tput sgr 0)]\n" \
    $(tput setaf 1)$(tput setab 0)"\n"\
    $(tput setaf 1)$(tput setab 0)$*$(tput sgr 0)
}

checkMsg() {
    echo -e "" \
    "$(tput setaf 2)[x] $*$(tput sgr 0)"
}
checkWarningMsg() {
    echo -e "" \
    "$(tput setaf 3)[!] $*$(tput sgr 0)"
}
checkNotMsg() {
    echo -e $(tput setaf 1)$(tput setab 0)"" \
    "$(tput setaf 1)$(tput setab 0)[-] $*$(tput sgr 0)"
}

runMsg() {
    echo -e "" \
    "[run] '$*' [$(tput setaf 3)$(tput setab 0)${BASH_LINENO}$(tput sgr 0)]"
}

logMsg() {
    echo -e "" \
    "$*"
}

infoMsg() {
    echo -e "" \
    "\n" \
    "$*"
}

exitMsg() {
    errorMsg "[EXIT]\n" \
    "Good by'"

    cd t_deploy
}

warningMsg() {
    echo -e "" \
    "$(tput setaf 3)$(tput setab 0)$*$(tput sgr 0)"
}

infoMsg "Hello!"

if [ "$#" -lt 1 ] || [ "$#" -lt 2 ] || [ "$#" -lt 3 ]|| [ "$#" -lt 4 ]; then
    errorMsg "Missing argument\n" \
    "Feel free to read the manual by executing 'make help'"
    exit
fi

DRY=""
if [ "$#" -lt 5 ]; then
    DRY=""
else 
    if [ "$5" = "dry" ]; then
    DRY="$5"
    fi
fi

#DRY="dry"
deploy $1 $2 $3 $4 $DRY