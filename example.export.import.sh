#!/bin/bash

## 
## export
##
mysqldump --defaults-extra-file=<(printf "[client]\nuser = %s\npassword = %s\nhost = %s" "%%%SOURCE_USERNAME%%%" "'%%%SOURCE_PASSWORD%%%'" "%%%SOURCE_HOST%%%") -v %%%SOURCE_DATABASE%%% %%%IGNORE_TABLES%%% --single-transaction --skip-add-locks --no-autocommit > mysqldump_%%%SOURCE_DATABASE%%%_%%%TIME%%%.sql;

## 
## import
##
nohup mysql --defaults-extra-file=<(printf "[client]\nuser = %s\npassword = %s\nhost = %s" "%%%TARGET_USERNAME%%%" "'%%%TARGET_PASSWORD%%%'" "%%%TARGET_HOST%%%") %%%TARGET_DATABASE%%% --default-character-set=utf8 1>trash/%%%TIME%%%/stdout.txt 2>trash/%%%TIME%%%/stderr.txt < mysqldump_%%%SOURCE_DATABASE%%%_%%%TIME%%%.sql &;

## 
## show ID of last background task
##
echo "[$!]";